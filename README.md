# International Assessments in Python 🎓

## Installation Guide
- Preferably use Python >= 3.6
- Install packages from requirements.txt; clone this repo, open terminal and run the following command:
`pip install -r requirements.txt`
or if you are conda user:
`conda install --file requirements.txt`
- At the moment this package provides you with the basic functionalities so if you have the following packages and their dependencies installed you should be ready to go:
  - pandas=1.1.5
  - numpy=1.19.5
  - statsmodels=0.12.2
